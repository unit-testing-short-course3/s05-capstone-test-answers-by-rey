const { exchangeRates } = require("../src/util.js");

module.exports = (app) => {
  app.get("/", (req, res) => {
    return res.send({ data: {} });
  });

  app.get("/rates", (req, res) => {
    return res.send({
      rates: exchangeRates,
    });
  });

  app.post("/currency", (req, res) => {
    //create if conditions here to check the currency

    if (!req.body.hasOwnProperty("name")) {
      return res
        .status(400)
        .send({ Error: "Bad request - missing name parameter" });
    }

    if (typeof req.body["name"] !== "string") {
      return res
        .status(400)
        .send({ Error: "Bad request - name parameter is not a string" });
    }

    if (req.body["name"].length === 0) {
      return res
        .status(400)
        .send({ Error: "Bad request - name parameter is empty" });
    }

    if (!req.body.hasOwnProperty("ex")) {
      return res
        .status(400)
        .send({ Error: "Bad request - missing ex parameter" });
    }

    if (typeof req.body["ex"] !== "object") {
      return res
        .status(400)
        .send({ Error: "Bad request - name parameter is not an object" });
    }

    if (req.body["ex"].length === 0) {
      return res
        .status(400)
        .send({ Error: "Bad request - ex parameter is empty" });
    }

    if (!req.body.hasOwnProperty("alias")) {
      return res
        .status(400)
        .send({ Error: "Bad request - missing alias parameter" });
    }

    if (typeof req.body["alias"] !== "string") {
      return res
        .status(400)
        .send({ Error: "Bad request - alias parameter is not a string" });
    }

    if (req.body["alias"].length === 0) {
      return res
        .status(400)
        .send({ Error: "Bad request - alias parameter is empty" });
    }

    const conditions =
      !req.body.hasOwnProperty("name") &&
      typeof req.body["name"] !== "string" &&
      req.body["name"].length === 0 &&
      !req.body.hasOwnProperty("ex") &&
      typeof req.body["ex"] !== "object" &&
      req.body["ex"].length === 0 &&
      !req.body.hasOwnProperty("alias") &&
      typeof req.body["alias"] !== "string" &&
      req.body["alias"].length === 0;

    if (!conditions && exchangeRates.hasOwnProperty(req.body["alias"])) {
      return res
        .status(400)
        .send({
          Error:
            "Bad request - all fields are not complete or alias parameter has a duplicate",
        });
    }

    if (!conditions && !exchangeRates.hasOwnProperty(req.body["alias"])) {
      return res
        .status(200)
        .send({
          Error:
            "Bad request - all fields are complete or alias parameter has no duplicate",
        });
    }

    //return status 200 if route is running
    return res.status(200).send({
      Message: "Route is running",
    });
  });
};
