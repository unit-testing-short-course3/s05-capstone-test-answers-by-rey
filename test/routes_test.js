const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");
chai.use(http);

describe("forex_api_test_suite", (done) => {
  //add the solutions here
  const domain = "http://localhost:5001";

  it("POST /currency is running", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 'riyadh',
		name: 'Saudi Arabian Riyadh',
		ex: {
		'peso': 0.47,
        'usd': 0.0092,
        'won': 10.93,
        'yuan': 0.065
	}
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });


  it("POST /currency name is missing", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 'riyadh',       
        ex: {
			'peso': 0.47,
			'usd': 0.0092,
			'won': 10.93,
			'yuan': 0.065
		}
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency name is not a string", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 'riyadh', 
		name: 1,      
        ex: {
			'peso': 0.47,
			'usd': 0.0092,
			'won': 10.93,
			'yuan': 0.065
		}
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency name is empty", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 'riyadh', 
		name: '',      
        ex: {
			'peso': 0.47,
			'usd': 0.0092,
			'won': 10.93,
			'yuan': 0.065
		}
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency ex is missing", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 'riyadh', 
		name: 'Saudi Arabian Riyadh'
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency ex is not an object", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 'riyadh', 
		name: 'Saudi Arabian Riyadh',
		ex: "not an object"
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency ex is empty", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 'riyadh', 
		name: 'Saudi Arabian Riyadh',
		ex: []
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency alias is missing", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
		name: 'Saudi Arabian Riyadh',
		ex: {
			'peso': 0.47,
			'usd': 0.0092,
			'won': 10.93,
			'yuan': 0.065
		}
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency alias is not a string", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 1, 
		name: 'Saudi Arabian Riyadh',      
        ex: {
			'peso': 0.47,
			'usd': 0.0092,
			'won': 10.93,
			'yuan': 0.065
		}
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency alias is empty", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: '', 
		name: 'Saudi Arabian Riyadh',      
        ex: {
			'peso': 0.47,
			'usd': 0.0092,
			'won': 10.93,
			'yuan': 0.065
		}
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency all fields are not complete or alias has a duplicate", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 'usd', 
		name: 'Saudi Arabian Riyadh',      
        ex: {
			'peso': 0.47,
			'usd': 0.0092,
			'won': 10.93,
			'yuan': 0.065
		}
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("POST /currency all fields are complete and alias has no duplicate", (done) => {
    chai
      .request(domain)
      .post("/currency")
      .type("json")
      .send({
        alias: 'riyadh', 
		name: 'Saudi Arabian Riyadh',      
        ex: {
			'peso': 0.47,
			'usd': 0.0092,
			'won': 10.93,
			'yuan': 0.065
		}
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });
});
